declare module 'mocha-logger' {
  export default class mlog {
    static log(s: string): void;
    static pending(s: string): void;
    static success(s: string): void;
    static error(s: string): void;
  }
}